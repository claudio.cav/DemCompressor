// DemCompressor.cpp : definisce il punto di ingresso dell'applicazione console.
//

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
#include <windows.h> 
#else
#include <dirent.h>
#endif
#include <sys/stat.h>
#include <stdio.h>
#include <iostream>
#include <vector>
#include "include/cmd-parser/parser.hpp"
#include <fstream>
#include <functional>
#define MINIZ_NO_ZLIB_COMPATIBLE_NAMES
#define MINIZ_NO_ARCHIVE_APIS
#include "include/miniz.c"
using namespace std;

void nextForward(int direction, int pos, short oldValue);
std::string trim(std::string& str, const char& val);
std::vector<std::uint8_t> compress(std::string filename, short step, short compressionType);
void compresser(std::string filename, short compressionType);
void getFilesInDirectory(std::vector<std::string> &out, const std::string &directory);
//void getDir(const char* d, std::vector<std::string> & f, const char point, const char hpoint);
size_t hgtFormat = 3;
std::vector<std::int16_t> hgt;

int main(int argc, char const *argv[]) {
	optionparser::parser parser;

	parser.add_option("--resolution", "-r").help("resolution of the dems (1200/3600)")
		.mode(optionparser::store_value)
		.default_value("3")
		.required(true);
	parser.add_option("--dem", "-d").help("directory with the DEM folder (with single baskslash \ )")
		.mode(optionparser::store_value)
		.default_value("./").required(true);
	parser.eat_arguments(argc, argv);

	std::string demFolder = parser.get_value<std::string>("dem");
	std::cout << "DEM Folder: " << demFolder << std::endl;

	hgtFormat = parser.get_value<int>("resolution");

	if(hgtFormat == 1)
	hgtFormat = 3600;
	else if (hgtFormat == 3)
	hgtFormat = 1200;
	else hgtFormat = 1200;
	std::cout << "Resolution: " << hgtFormat << std::endl;
	hgt.resize((hgtFormat) *(hgtFormat), 0);

	std::vector<std::string> files;
getFilesInDirectory(files,demFolder.c_str());
	std::vector<std::string>::const_iterator it = files.begin();

	while (it != files.end())
	{
		compresser(*it, 3);
		it++;
	}
}


void compresser(std::string filename, short compressionType) {
	std::string fileout = trim(filename, '.');

	std::vector<std::uint8_t> hgt1step = compress(filename, 1, compressionType);

	std::ofstream f(fileout + ".cgt", std::ifstream::out | std::ifstream::binary);
	std::copy(hgt1step.begin(), hgt1step.end(), std::ostreambuf_iterator<char>(f));
	f.close();
}


std::vector<std::uint8_t> compress(std::string filename, short step, short compressionType) {

	try {

		std::vector<int16_t>().swap(hgt);
		hgt.resize(hgtFormat*hgtFormat, 0);
		std::vector<std::uint8_t> filevector(hgtFormat*hgtFormat * 2, 0);

		// read file byte per byte
		std::ifstream file(filename, std::ifstream::in | std::ifstream::binary);
		if (file.good()) {
			file.ignore(hgtFormat * 2 + 2);
			for (size_t position = 0; position < filevector.size(); position += hgtFormat * 2) {
				file.read(reinterpret_cast<char*>(&filevector.at(position)), hgtFormat * 2);
				file.ignore(2);
			}
		}
		file.close();


		//std::transform(filevector.begin(), filevector.end(), hgt.begin(), [](std::int8_t i, std::int8_t j, std::int16_t x) {return x = (j<<8 | i ); });
		int maxPos = 0;
		int max = 0;

		//obtain starting point
		for (int i = 0, j = 0; j < hgtFormat * hgtFormat; i = i + 2, j++)
		{
			hgt[j] = filevector[i] << 8 | filevector[i + 1];

				if (hgt[j] > max)
				{
					max = hgt[j];
					maxPos = j;
				}

		}

	int pmin = *std::min_element(hgt.begin(), hgt.end());
//int pmax = *std::max_element(hgt.begin(), hgt.end());
		if (step == 1) {
			//--------------------------------
			nextForward(0, maxPos, max);

			if ((maxPos + 1) % hgtFormat != 0) {
				nextForward(1, maxPos, max);
				nextForward(2, maxPos, max);
				nextForward(3, maxPos, max);
			}
			nextForward(4, maxPos, max);
			if (maxPos % hgtFormat != 0) {
				nextForward(5, maxPos, max);
				nextForward(6, maxPos, max);
				nextForward(7, maxPos, max);
			}
		}

	
		short min = *std::min_element(hgt.begin(), hgt.end());
		std::cout << filename << " -> Max Point: " << max << " Min Point: " << pmin << " Max Delta: (" << min << ")\n";
		if(min<0) min = -min;
		transform(hgt.begin(), hgt.end(), hgt.begin(), std::bind2nd(std::plus<short>(), min));

		//swipe the bytes
		std::transform(hgt.begin(), hgt.end(), filevector.begin(), [](std::int16_t i) {return reinterpret_cast<std::uint8_t const *>(&i)[0]; });
		std::transform(hgt.begin(), hgt.end(), filevector.begin() + hgt.size(), [](std::int16_t i) {return reinterpret_cast<std::uint8_t const *>(&i)[1]; });


		//fastlz
		if (compressionType == 3) {

			std::vector<std::uint8_t> x(mz_compressBound(filevector.size()), 0);
			mz_ulong len = x.size();
			int err = mz_compress2(reinterpret_cast<unsigned char*>(&x.at(0)), &len, reinterpret_cast<unsigned char*>(&filevector.at(0)), filevector.size(), MZ_BEST_COMPRESSION);
			x.resize(len);

			//HEADER
			x.insert(x.begin(), (min >> 8) & 0xFF);
			x.insert(x.begin(), min & 0xFF);
			x.insert(x.begin(), (maxPos >> 24) & 0xFF);
			x.insert(x.begin(), (maxPos >> 16) & 0xFF);
			x.insert(x.begin(), (maxPos >> 8) & 0xFF);
			x.insert(x.begin(), maxPos & 0xFF);

			return x;
		}

	}
 catch (const char* msg) {
	 std::cerr << msg << std::endl;
 }
}

void nextForward(int direction, int pos, short oldValue)
{
	int ctrl = 0;
	switch (direction)
	{
	case 0:
		ctrl = pos - hgtFormat;
		break;
	case 1:
		ctrl = pos - hgtFormat + 1;
		break;
	case 2:
		ctrl = pos + 1;
		break;
	case 3:
		ctrl = pos + hgtFormat + 1;
		break;
	case 4:
		ctrl = pos + hgtFormat;
		break;
	case 5:
		ctrl = pos + hgtFormat - 1;
		break;
	case 6:
		ctrl = pos - 1;
		break;
	case 7:
		ctrl = pos - hgtFormat - 1;
		break;
	}

	if (ctrl >= 0 && ctrl < hgtFormat * hgtFormat)
	{
		short value = hgt[ctrl];
		/*if (hgt[ctrl] > 9000) {
			value = oldValue;
			hgt[ctrl] = oldValue;
		}*/

		hgt[ctrl] -= oldValue;
		switch (direction)
		{
		case 1:
			nextForward(0, ctrl, value);
			if ((ctrl + 1) % hgtFormat != 0)
			{
				nextForward(2, ctrl, value);
				nextForward(1, ctrl, value);
			}
			break;
		case 2:
			if ((ctrl + 1) % hgtFormat != 0) nextForward(2, ctrl, value);
			break;
		case 3:
			nextForward(4, ctrl, value);
			if ((ctrl + 1) % hgtFormat != 0)
			{
				nextForward(2, ctrl, value);
				nextForward(3, ctrl, value);
			}
			break;
		case 5:
			nextForward(4, ctrl, value);
			if (ctrl  % hgtFormat != 0)
			{
				nextForward(6, ctrl, value);
				nextForward(5, ctrl, value);
			}
			break;
		case 6:
			if (ctrl % hgtFormat != 0) nextForward(6, ctrl, value);
			break;
		case 7:
			nextForward(0, ctrl, value);
			if (ctrl % hgtFormat != 0)
			{
				nextForward(6, ctrl, value);
				nextForward(7, ctrl, value);
			}
			break;
		default:
			nextForward(direction, ctrl, value);
			break;
		}
	}

}
std::string trim(std::string& str, const char& val)
{
	size_t first = str.find_first_not_of(val);
	size_t last = str.find_first_of(val);
	return str.substr(first, last);

}


/*
void getDir(const char* d, std::vector<std::string> & f, const char point, const char hpoint)
{
	FILE* pipe = NULL;
	std::string pCmd = "dir /B /S " + std::string(d);
	char buf[256];
std::cout << pCmd;
	if (NULL == (pipe = popen(pCmd.c_str(), "rt")))
	{
		return;
	}

std::cout << pipe;
	while (!feof(pipe))
	{
		if (fgets(buf, 256, pipe) != NULL)
		{
			std::string a = std::string(buf);
			std::string sbase = trim(a, '\n');
			char pointchar = sbase.at(sbase.length() - 4);
			char pointh = sbase.at(sbase.length() - 3);
			if (pointchar == point && pointh == hpoint)
				f.push_back(sbase);
		}
	}
	pclose(pipe);
}*/

void getFilesInDirectory(std::vector<string> &out, const string &directory)
{

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)



    HANDLE dir;
    WIN32_FIND_DATA file_data;

    if ((dir = FindFirstFile((directory + "/*").c_str(), &file_data)) == INVALID_HANDLE_VALUE)
        return; /* No files found */

    do {
        const string file_name = file_data.cFileName;
        const string full_file_name = directory + "/" + file_name;
        const bool is_directory = (file_data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0;

        if (file_name[0] == '.')
            continue;

	if(file_name.substr( file_name.length() - 4 ) != ".hgt")
	    continue;

        if (is_directory)
            continue;

        out.push_back(full_file_name);
    } while (FindNextFile(dir, &file_data));

    FindClose(dir);
#else


    DIR *dir;
    class dirent *ent;
    class stat st;

    dir = opendir(directory.c_str() );
    while ((ent = readdir(dir)) != NULL) {
        const string file_name = ent->d_name;
        const string full_file_name = directory + "/" + file_name;

        if (file_name[0] == '.')
            continue;

	if(file_name.substr( file_name.length() - 4 ) != ".hgt")
	    continue;

        if (stat(full_file_name.c_str(), &st) == -1)
            continue;

        const bool is_directory = (st.st_mode & S_IFDIR) != 0;

        if (is_directory)
            continue;

        out.push_back(full_file_name);
    }
    closedir(dir);
#endif

} 
